use std::marker::PhantomData;

use bevy::prelude::*;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins.set(ImagePlugin::default_nearest()))
        .add_state::<AppState>()
        .add_state::<PlayState>()
        .init_resource::<SpriteHandles>()
        .add_system(load_textures.in_schedule(OnEnter(AppState::Setup)))
        .add_system(check_textures.in_set(OnUpdate(AppState::Setup)))
        .add_system(finish_texture_atlas.in_schedule(OnExit(AppState::Setup)))
        .add_system(prepare_game.in_schedule(OnEnter(AppState::Playing)))
        .add_system(
            cooldown::<Velocity>
                .in_set(OnUpdate(AppState::Playing))
                .in_set(OnUpdate(PlayState::Playing))
                .before(movement_input),
        )
        .add_system(
            cooldown::<Asteroid>
                .in_set(OnUpdate(AppState::Playing))
                .in_set(OnUpdate(PlayState::Playing))
                .before(movement_input),
        )
        .add_system(
            cooldown::<Bullet>
                .in_set(OnUpdate(AppState::Playing))
                .in_set(OnUpdate(PlayState::Playing))
                .before(movement_input),
        )
        .add_system(
            despawn_bullets
                .in_set(OnUpdate(AppState::Playing))
                .in_set(OnUpdate(PlayState::Playing))
                .before(movement_input),
        )
        .add_system(
            movement_input
                .in_set(OnUpdate(AppState::Playing))
                .in_set(OnUpdate(PlayState::Playing))
                .before(movement),
        )
        .add_system(
            asteroid_shot
                .in_set(OnUpdate(AppState::Playing))
                .in_set(OnUpdate(PlayState::Playing))
                .before(movement),
        )
        .add_system(
            spawn_asteroids
                .in_set(OnUpdate(AppState::Playing))
                .in_set(OnUpdate(PlayState::Playing))
                .before(movement),
        )
        .add_system(
            fire.in_set(OnUpdate(AppState::Playing))
                .in_set(OnUpdate(PlayState::Playing))
                .before(movement),
        )
        .add_system(
            movement
                .in_set(OnUpdate(AppState::Playing))
                .in_set(OnUpdate(PlayState::Playing))
                .before(drag),
        )
        .add_system(
            drag.in_set(OnUpdate(AppState::Playing))
                .in_set(OnUpdate(PlayState::Playing))
                .before(position),
        )
        .add_system(
            position
                .in_set(OnUpdate(AppState::Playing))
                .in_set(OnUpdate(PlayState::Playing)),
        )
        .run();
}

#[derive(Debug, Clone, Copy, Default, PartialEq, Eq, Hash, States)]
enum PlayState {
    #[default]
    Playing,
    PoweUp,
    Menu,
}

#[derive(Debug, Clone, Copy, Default, PartialEq, Eq, Hash, States)]
enum AppState {
    #[default]
    Setup,
    Menu,
    Playing,
}

#[derive(Debug, Resource, Default)]
struct SpriteHandles {
    ship: Handle<Image>,
    asteroid: Handle<Image>,
    bullet: Handle<Image>,
}

#[derive(Debug, Resource, Default)]
struct SpriteIndices {
    ship: usize,
    asteroid: usize,
    bullet: usize,
}

fn load_textures(mut handles: ResMut<SpriteHandles>, asset_server: Res<AssetServer>) {
    info!("loading textures");

    handles.asteroid = asset_server.load("textures/asteroid_large.png");
    handles.bullet = asset_server.load("textures/bullet.png");
    handles.ship = asset_server.load("textures/ship.png");
}

fn check_textures(
    handles: Res<SpriteHandles>,
    asset_server: Res<AssetServer>,
    mut next_state: ResMut<NextState<AppState>>,
) {
    if asset_server.get_load_state(&handles.ship) != bevy::asset::LoadState::Loaded {
        return;
    }

    if asset_server.get_load_state(&handles.bullet) != bevy::asset::LoadState::Loaded {
        return;
    }
    if asset_server.get_load_state(&handles.asteroid) != bevy::asset::LoadState::Loaded {
        return;
    }
    next_state.set(AppState::Playing);
}

#[derive(Debug, Resource)]
struct Atlas(Handle<TextureAtlas>);

fn finish_texture_atlas(
    mut textures: ResMut<Assets<Image>>,
    handles: Res<SpriteHandles>,
    asset_server: Res<AssetServer>,
    mut commands: Commands,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
) {
    info!("finished loading");

    let mut builder = TextureAtlasBuilder::default();

    add_texture(&mut textures, &handles.ship, &mut builder, &asset_server);

    add_texture(&mut textures, &handles.bullet, &mut builder, &asset_server);
    add_texture(
        &mut textures,
        &handles.asteroid,
        &mut builder,
        &asset_server,
    );
    let texture_atlas = builder.finish(&mut textures).unwrap();

    let indices = SpriteIndices {
        ship: texture_atlas.get_texture_index(&handles.ship).unwrap(),
        bullet: texture_atlas.get_texture_index(&handles.bullet).unwrap(),
        asteroid: texture_atlas.get_texture_index(&handles.asteroid).unwrap(),
    };

    commands.insert_resource(indices);
    commands.insert_resource(Atlas(texture_atlases.add(texture_atlas)));
}

fn add_texture(
    textures: &mut Assets<Image>,
    handle: &Handle<Image>,
    builder: &mut TextureAtlasBuilder,
    asset_server: &AssetServer,
) {
    let Some(texture) = textures.get(handle) else {
        warn!("Texture {:?} could not be loaded", asset_server.get_handle_path(handle));
        return
    };

    builder.add_texture(handle.clone(), texture);
}

fn prepare_game(mut commands: Commands, atlas: Res<Atlas>, indices: Res<SpriteIndices>) {
    info!("starting game");

    commands.spawn(Spawner { spawn_rate: 5.0 });

    commands.spawn((
        SpriteSheetBundle {
            sprite: TextureAtlasSprite::new(indices.ship),
            texture_atlas: atlas.0.clone(),
            transform: Transform::from_xyz(0., 0., 0.),
            ..Default::default()
        },
        Player,
        Ship::default(),
        Velocity::default(),
        Rotation::default(),
        Position(Vec2::splat(0.)),
    ));

    commands.spawn(Camera2dBundle::default());
}

fn spawn_asteroids(
    mut commands: Commands,
    atlas: Res<Atlas>,
    indices: Res<SpriteIndices>,
    spawner: Query<(&Spawner, Entity), Without<Cooldown<Asteroid>>>,
) {
    for (spawner, entity) in spawner.iter() {
        commands.spawn((
            SpriteSheetBundle {
                sprite: TextureAtlasSprite::new(indices.asteroid),
                texture_atlas: atlas.0.clone(),
                transform: Transform::from_xyz(0., 0., 0.),
                ..Default::default()
            },
            Asteroid { size: 4 },
            Velocity::default(),
            Rotation::default(),
            Position(Vec2::splat(0.)),
        ));
        commands
            .entity(entity)
            .insert(Cooldown::<Asteroid>::new(spawner.spawn_rate));
    }
}

fn asteroid_shot(
    asteroids: Query<(Entity, &Asteroid, &Position)>,
    mut bullets: Query<(Entity, &mut Bullet, &Position)>,
    mut commands: Commands,
) {
    for (a_entity, asteroid, a_position) in asteroids.iter() {
        let asteroid_radius = (asteroid.size as f32) * 0.0025;
        for (b_entity, mut bullet, b_position) in bullets.iter_mut() {
            if (a_position.0 - b_position.0).length_squared() <= asteroid_radius
                && bullet.piercing > 0
            {
                bullet.piercing -= 1;
                if bullet.piercing == 0 {
                    commands.entity(b_entity).despawn();
                }

                commands.entity(a_entity).despawn();

                break;
            }
        }
    }
}

fn fire(
    input: Res<Input<KeyCode>>,
    mut commands: Commands,
    mut player: Query<
        (&Rotation, &Ship, &Position, Entity),
        (With<Player>, Without<Cooldown<Bullet>>),
    >,
    atlas: Res<Atlas>,
    indices: Res<SpriteIndices>,
) {
    if input.pressed(KeyCode::Space) {
        for (rotation, ship, position, entity) in player.iter_mut() {
            commands
                .entity(entity)
                .insert(Cooldown::<Bullet>::new(ship.fire_rate));

            let velocity = Vec2::from_angle(rotation.0 * std::f32::consts::TAU) * ship.bullet_speed;
            commands.spawn((
                SpriteSheetBundle {
                    sprite: TextureAtlasSprite::new(indices.bullet),
                    texture_atlas: atlas.0.clone(),
                    transform: Transform::from_xyz(0., 0., 0.),
                    ..Default::default()
                },
                Bullet {
                    piercing: ship.piercing,
                },
                Velocity(velocity),
                Position(position.0),
                Rotation(rotation.0),
                Cooldown::<Bullet>::new(ship.bullet_dur),
            ));
        }
    }
}

fn despawn_bullets(
    bullets: Query<Entity, (With<Bullet>, Without<Cooldown<Bullet>>)>,
    mut commands: Commands,
) {
    for entity in bullets.iter() {
        commands.entity(entity).despawn();
    }
}

fn movement_input(
    input: Res<Input<KeyCode>>,
    mut player: Query<(&mut Velocity, &mut Rotation, &Ship, Entity), With<Player>>,
    time: Res<Time>,
    mut commands: Commands,
) {
    let mut rotation = 0.0;
    let mut acceleration = 0.0;
    let mut deceleration = 0.0;
    let delta_t = time.delta_seconds();

    let mut add_drag = false;

    if input.pressed(KeyCode::Left) {
        rotation += delta_t;
        add_drag = true;
    }
    if input.pressed(KeyCode::Right) {
        rotation -= delta_t;
        add_drag = true;
    }
    if input.pressed(KeyCode::Up) {
        acceleration = delta_t;
        add_drag = true;
    }
    if input.pressed(KeyCode::Down) {
        deceleration = delta_t;
        add_drag = true;
    }

    for (mut v, mut r, ship, e) in player.iter_mut() {
        r.0 += rotation * ship.turn_rate;
        r.0 %= 1.0;

        let velocity = acceleration * ship.acceleration - (deceleration * ship.deceleration);
        let add = Vec2::from_angle(r.0 * std::f32::consts::TAU) * velocity;
        v.0 += add;
        v.0 = v.0.clamp_length_max(ship.max_speed);

        if add_drag {
            commands
                .entity(e)
                .insert((Cooldown::<Velocity>::new(ship.drag_dur), StartVelocity(v.0)));
        }
    }
}

#[derive(Debug, Component)]
struct StartVelocity(Vec2);

fn drag(mut player: Query<(&mut Velocity, &Cooldown<Velocity>, &StartVelocity)>) {
    for (mut v, drag, start_velocity) in player.iter_mut() {
        v.0 = start_velocity.0.lerp(Vec2::ZERO, drag.current / drag.total);
    }
}

fn movement(mut player: Query<(&Velocity, &mut Position)>, time: Res<Time>) {
    for (v, mut p) in player.iter_mut() {
        p.0 += v.0 * time.delta_seconds();

        while p.0.x <= 1.0 {
            p.0.x += 2.0
        }

        while p.0.y <= 1.0 {
            p.0.y += 2.0
        }
        while p.0.x >= 1.0 {
            p.0.x -= 2.0
        }

        while p.0.y >= 1.0 {
            p.0.y -= 2.0
        }
    }
}

fn cooldown<T>(
    mut cooldowns: Query<(&mut Cooldown<T>, Entity)>,
    time: Res<Time>,
    mut commands: Commands,
) {
    for (mut cooldown, entity) in cooldowns.iter_mut() {
        if cooldown.update(&time) {
            commands.entity(entity).remove::<Cooldown<T>>();
        }
    }
}

fn position(
    mut positions: Query<(&mut Transform, &Position, &Rotation)>,
    camera: Query<&OrthographicProjection, With<Camera2d>>,
) {
    let camera = camera.single();

    let area = camera.area;
    let size = area.half_size();

    for (mut t, pos, rot) in positions.iter_mut() {
        let pos = (pos.0 * size).extend(0.0);

        t.translation = pos;
        t.rotation = Quat::from_rotation_z(rot.0 * std::f32::consts::TAU)
    }
}

#[derive(Debug, Component)]
struct Spawner {
    spawn_rate: f32,
}

#[derive(Debug, Component)]
struct Tint(Color);

#[derive(Debug, Component)]
struct Position(Vec2);

#[derive(Debug, Component, Default)]
struct Rotation(f32);

#[derive(Debug, Component)]
struct Player;

#[derive(Debug, Component)]
struct Ship {
    max_speed: f32,
    acceleration: f32,
    deceleration: f32,
    fire_rate: f32,
    bullet_speed: f32,
    bullet_dur: f32,
    turn_rate: f32,
    piercing: u8,
    drag_dur: f32,
}

impl Default for Ship {
    fn default() -> Self {
        Self {
            max_speed: 0.5,
            acceleration: 1.0,
            deceleration: 0.5,
            bullet_speed: 0.5,
            fire_rate: 1.0,
            turn_rate: 1.0,
            piercing: 1,
            drag_dur: 2.0,
            bullet_dur: 2.0,
        }
    }
}

#[derive(Debug, Component)]
struct Cooldown<T: 'static> {
    current: f32,
    total: f32,
    _p: PhantomData<T>,
}

unsafe impl<T> Send for Cooldown<T> {}
unsafe impl<T> Sync for Cooldown<T> {}

impl<T> Cooldown<T> {
    pub fn new(dur: f32) -> Self {
        Self {
            current: 0.0,
            total: dur,
            _p: Default::default(),
        }
    }

    pub fn update(&mut self, time: &Time) -> bool {
        self.current += time.delta_seconds();
        self.current > self.total
    }
}

#[derive(Debug, Component)]
struct Despawn(f32);

#[derive(Debug, Component, Default)]
struct Velocity(Vec2);

#[derive(Debug, Component)]
struct Bullet {
    piercing: u8,
}

#[derive(Debug, Component)]
struct Asteroid {
    size: u8,
}
